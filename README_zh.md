# rebound

# 简介

rebound是一个模拟弹簧动力学，用于驱动物理动画的库。

## 下载安装

```javascript
ohpm install @ohos/rebound
```

OpenHarmony ohpm环境配置等更多内容，请参考[如何安装OpenHarmony ohpm](https://gitcode.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)



# 使用说明
```
import rebound from '@ohos/rebound';
```

功能一：创建维护弹簧对象的弹簧系统。通过配置张力、摩擦、动画值来创建和注册弹簧对象，并添加监听器以接收弹簧位置。

```javascript
spring: rebound.Spring = springSystem.createSpring();
spring.setSpringConfig(rebound.SpringConfig.DEFAULT_ORIGAMI_SPRING_CONFIG);
springConfig: rebound.SpringConfig = spring.getSpringConfig();
spring.addListener({
  onSpringUpdate: function(spring) {
    var val = spring.getCurrentValue();
    //Use Spring val to Process/Animate
  },
  onSpringEndStateChange: function(spring){
  }
});
```

功能二：可以设置弹簧动画在0到n之间运行，n是结束值，调用Spring.setEndValue()设置结束值。在以下代码中，弹簧位置在0到1之间变化。

```javascript
spring.setEndValue(1);
```

功能三：利用弹簧位置的不断变化动态改变视图组件的位置，从而为视图组件创造出动画效果。调用mapValueInRange()将弹簧位置转换为自定义范围。在下面的示例中，弹簧位置转换为图像大小缩放比来实现动画效果。

```javascript
spring.addListener({
  onSpringUpdate: function(spring) {
    var val = spring.getCurrentValue();
    val = rebound.MathUtil
    .mapValueInRange(val, 0, 1, 1, 0.5);
    self.imageSize=val;
  },
  onSpringEndStateChange: function(spring){
  }
});
```

功能四：调用spring.setEndValue(0)结束弹簧动画效果

```javascript
spring.setEndValue(0);
```



### 接口说明

#### SpringSystem---弹簧系统

| 接口名                               | 参数                              | Return Value  | 说明                                                         |
| ------------------------------------ | ------------------------------- | ------------- | ------------------------------------------------------------ |
| SpringSystem()                       | Looper                          | StringSystem  | 弹簧系统由循环器驱动，当弹簧系统处于闲置状态的时候，循环器负责执行每一帧。弹簧系统里包含有三种循环器：动画循环器、模拟循环器和步进循环器。其中动画循环器由于经常用于UI动画被设置为默认循环器。 |
| createSpring()                       | tension?: number, friction?: number | Spring        | 给弹簧系统添加一个新弹簧。这个弹簧将在物理迭代循环中被执行。默认情况下，弹簧将使用默认的Origami 弹簧配置，张力为40，摩擦力为7，但是您也可以在这里提供自己的值。 |
| createSpringWithBouncinessAndSpeed() | bounciness?: number, speed?: number | Spring        | 添加具有指定弹力和速度的弹簧。要复制基于PopAnimation补丁的组合折叠效果，可使用此工厂方法创建匹配的弹簧。 |
| createSpringWithConfig():            | kwargs: SpringConfig            | Spring        | 使用提供的SpringConfig添加一个弹簧。                         |
| getIsIdle()                          | None                            | boolean       | 检查弹簧系统是空闲的还是活动的。如果弹簧系统中的所有弹簧都处于静止状态，即物理力已经达到平衡，那么这个方法将返回true。 |
| getSpringById()                      | id: number                      | Spring        | 通过id从弹簧系统中查找并获取特定的弹簧。可用于在弹簧系统中的组合循环器执行之前或之后检查弹簧的状态。 |
| getAllSprings()                      | None                            | Array<Spring> | 获取在弹簧系统中注册的所有弹簧的列表。                       |
| registerSpring()                     | value: Spring                   | void          | 手动向该系统添加弹簧。如果弹簧是用弹簧系统的createSpring创建的，则会自动调用本方法。本方法生成的弹簧会自动注册，以便弹簧系统去执行它。 |
| deregisterSpring(value: Spring)      | value: Spring                   | void          | 弹簧系统注销某个弹簧。一旦被调用，弹簧系统将不在循环器中执行本弹簧。本方法通常是在调用弹簧的destroy方法时时自动调用。 |
| loop()                               | currentTimeMillis: number       | void          | 循环器每次执行移动相同距离。每次执行循环器的循环，会调用所有已经注册到弹簧系统的监听器的 onBeforeIntegrate。可以在每次迭代循环之前对弹簧做应该强制实施的任何约束或调整。 |
| activateSpring()                     | springId: number                | void          | 用于通知弹簧系统某个弹簧已经移除。如果当前循环器处于空闲状态，系统将启动循环器。 |




#### Spring---弹簧

| 接口名                                   | 参数                                          | 返回值           | 说明                                                                                                                                            |
| ---------------------------------------- | --------------------------------------------- |---------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| Spring()                                 | springSystem                                  | String        | 提供了一个经典弹簧的模型，用于模拟物体运动到静止的过程。                                                                                                                  |
| destroy()                                | None                                          | void          | 弹簧对象销毁回收。                                                                                                                                     |
| getId()                                  | None                                          | number        | 获取弹簧id                                                                                                                                        |
| getSpringConfig()                        | None                                          | SpringConfig  | 获取弹簧的配置参数。                                                                                                                                    |
| getCurrentValue()                        | None                                          | number        | 获取弹簧的当前位置。                                                                                                                                    |
| setCurrentValue()                        | currentValue: number, skipSetAtRest?: boolean | Spring        | 设置弹簧的当前位置。                                                                                                                                    |
| getStartValue()                          | None                                          | number        | 获取动画开始的位置。用于确定已发生的往复运动的次数。                                                                                                                    |
| getCurrentDisplacementDistance()         | None                                          | number        | 获得弹簧与其静止结束值的距离。                                                                                                                               |
| getEndValue()                            | None                                          | number        | 获取弹簧的终值或静止位置。                                                                                                                                 |
| setEndValue()                            | value: number                                 | Spring        | 设置弹簧的终值或静止位置。如果该值不同于当前值，弹簧系统将得到通知，并开始运行其循环，让弹簧达到静止状态。所有注册了onSpringEndStateChange的监听器也会立即收到此更新的通知。                                             |
| getVelocity()                            | None                                          | number        | 获取弹簧的当前速度，单位为像素/秒。                                                                                                                            |
| setVelocity()                            | value: number                                 | Spring        | 设置弹簧的当前速度，单位为像素/秒。直接手势操作时用于控制动画的执行。当不再用手势控制某个UI元素时，可以在动画弹簧上调用setVelocity，这样弹簧会以与手势结束时相同的速度继续运动。然后，弹簧的摩擦力、张力和位移将控制其运动，使其速度遵循现实中弹簧动力学的运动曲线而逐渐停止。 |
| getRestSpeedThreshold()                  | None                                          | number        | 获取此弹簧的静止速度阈值。                                                                                                                                 |
| setRestSpeedThreshold()                  | value: number                                 | Spring        | 为弹簧的移动速度设置一个阈值，弹簧速度低于该阈值时，将被视为处于静止状态。                                                                                                         |
| getRestDisplacementThreshold()           | None                                          | number        | 获取此弹簧的静止位移阈值。                                                                                                                                 |
| isOvershootClampingEnabled()             | None                                          | boolean       | 检查此弹簧是否启用越位移动。                                                                                                                                |
| setOvershootClampingEnabled()            | value: boolean                                | Spring        | 开启越位移动限制。当弹簧到达静止位置时，会立即停止，不管它现有的动量是多少。这对于某些不应该发生往复运动的动画类型很有用，例如缩放到0或alpha淡入淡出。                                                                |
| isOvershooting()                         | None                                          | boolean       | 通过比较弹簧开始时的移动方向与当前位置和终点值，检查弹簧是否已经越过终点。                                                                                                         |
| isAtRest()                               | None                                          | boolean       | 检查弹簧是否是AtRest，意味着它的当前值和结束值是相同的，并且它没有速度。前面描述的速度和位移的阈值定义了此等价检查的边界值。如果弹簧的张力为0，那么当它的速度低于静止速度阈值时，它将被认为是静止的                                         |
| setAtRest()                              | None                                          | Spring        | 设置弹簧状态为 At Rest                                                                                                                               |
| addListener()                            | value: Listener                               | Spring        | 向弹簧系统添加一个监听器，以接收集成前/后通知，允许弹簧被约束或调整。                                                                                                           |
| removeListener()                         | value: Listener                               | Spring        | 在弹簧系统上删除一个先前添加的监听器。                                                                                                                           |
| removeAllListeners()                     | None                                          | Spring        | 删除SpringSystem上所有以前添加的监听器。                                                                                                                    |
|                                          |                                               |               |                                                                                                                                               |

#### Listeners---监听器

| 接口名                   | 参数           | 返回值 | 说明                                                         |
| ------------------------ | -------------- | ------ | ------------------------------------------------------------ |
| onSpringEndStateChange() | spring: Spring | void   | 监听弹簧结束状态                                             |
| onBeforeIntegrate()      | spring: Spring | void   | onBeforeIntegrate在任何已经向弹簧系统注册了的监听器上调用。这使您有机会在每次循环之前对弹簧做应该强制实施的约束或调整。 |
| onAfterIntegrate()       | spring: Spring | void   | 集成步骤提前执行，保证任何已经注册到弹簧系统的监听器上可以调用onAfterIntegrate。这使您有机会在弹簧系统中的对弹簧上运行做任何约束或调整。 |
| onSpringActivate()       | spring: Spring | void   | 弹簧激活回调。                                               |
| onSpringUpdate           | spring: Spring | void   | 弹簧状态更新回调。                                           |
| onSpringAtRest()         | spring: Spring | void   | 弹簧停止运行回调。                                           |

#### SpringConfig---弹簧配置

| 接口名                            | 参数                              | 返回值       | 说明                                                         |
| --------------------------------- | --------------------------------- | ------------ | ------------------------------------------------------------ |
| SpringConfig()                    | tension: number, friction: number | SpringConfig | 创建一个没有张力的弹簧配置或一个有一定摩擦力的滑行弹簧，这样它就不会无限滑行。 |
| fromOrigamiTensionAndFriction()   | tension: number, friction: number | SpringConfig | 将Origami弹簧张力和摩擦力转换为反弹弹簧常数。如果正在用Origami做一个设计的原型，这很容易使的弹簧在反弹时表现完全相同。 |
| fromBouncinessAndSpeed            | bounciness: number, speed: number | SpringConfig | 将Origami PopAnimation弹簧弹力和速度转换为反弹弹簧常数。如果在Origami中使用PopAnimation补丁，这个工具将提供与原型相匹配的弹簧。 |
| coastingConfigWithOrigamiFriction | friction: number                  | SpringConfig | 创建一个没有张力的弹簧配置或一个有一定摩擦力的滑行弹簧，这样它就不会无限滑行。当将它和setVelocity一起使用时，可以让弹簧按照弹簧动力学的方式来运动。 |

#### Looper---循环器

| 接口名                     | 参数 | 返回值                   | 说明                                                         |
| -------------------------- | ---- | ------------------------ | ------------------------------------------------------------ |
| AnimationLooper()          | None | AnimationLooper          | 在动画计时循环中播放弹簧系统的每一帧。由于AnimationLooper是开发UI时常见的，所以它被设置为新弹簧系统的默认looper。 |
| SimulationLooper()         | None | SimulationLooper         | 同步生成预先录制的动画，随后可以在定时循环中播放。           |
| SteppingSimulationLooper() |      | SteppingSimulationLooper | 一次一步，由外部循环控制的循环器。                           |

#### OrigamiValueConverter---OrigamiValue转换器

| 接口名                     | 参数             | 返回值 | 说明                           |
| -------------------------- | ---------------- | ------ | ------------------------------ |
| tensionFromOrigamiValue()  | oValue: number   | number | Origami 值转换为张力的转换器。 |
| origamiValueFromTension()  | tension: number  | number | 张力转换为Origami 值的转换器。 |
| frictionFromOrigamiValue() | oValue: number   | number | Origami 值转换为摩擦的转换器。 |
| origamiFromFriction()      | friction: number | number | 摩擦转换为Origami 值的转换器。 |

#### MathUtil

| 接口名            | 参数                                                         | 返回值 | 说明                           |
| ----------------- | ------------------------------------------------------------ | ------ | ------------------------------ |
| mapValueInRange() | value: number, fromLow: number, fromHigh: number, toLow: number, toHigh: number | number | 将值从一个范围映射到另一个范围 |
| interpolateColor  | val: number, startColorStr: string, endColorStr: string      | string | 插值两种十六进制颜色           |
| degreesToRadians  | deg: number                                                  | number | 度数转换为 Randian值           |
| radiansToDegrees  | rad: number                                                  | number | Randian值转换为度数            |

#### util

| 接口名        | 参数             | 返回值 | 说明                                       |
| ------------- | ---------------- | ------ |------------------------------------------|
| bind()        | func, context    | void   | 将函数绑定到上下文对象                              |
| extend()      | target, source   | void   | 将源中的所有属性添加到目标中。                          |
| removeFirst() | array, item      | void   | 删除数组中首一个出现的引用。                           |
| hexToRGB()    | colorString      | string | 将十六进制格式的颜色字符串转换为它的等效rgb格式。执行颜色补间动画时非常方便。 |
| rgbToHex()    | rNum, gNum, bNum | string | rgb转十六进制                                 |

#### BouncyConversion--- 弹跳转换 

| I接口名            | 参数                              | 返回值           | 说明                                                         |
| ------------------ | --------------------------------- | ---------------- | ------------------------------------------------------------ |
| BouncyConversion() | bounciness: number, speed: number | BouncyConversion | 提供从Origami PopAnimation配置值转换为常规Origami张力和摩擦力值的数学方法。 |

## 关于混淆
- 代码混淆，请查看[代码混淆简介](https://docs.openharmony.cn/pages/v5.0/zh-cn/application-dev/arkts-utils/source-obfuscation.md)
- 如果希望rebound库在代码混淆过程中不会被混淆，需要在混淆规则配置文件obfuscation-rules.txt中添加相应的排除规则：
```
-keep
./oh_modules/@ohos/rebound
```

## 约束与限制
在下述版本验证通过：

DevEco Studio(5.0.3.122), SDK:API12 (5.0.0.17)。

DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)。

## 目录结构

```
|---- rebound  
|     |---- entry  # 示例代码文件夹
|     |---- rebound  # rebound 库文件夹
|           |---- rebound.js  # rebound对外接口
|     |---- README.MD  # 安装使用方法   
|     |---- README_zh.MD  # 安装使用方法                
```

## 贡献代码

使用过程中发现任何问题都可以提[Issue](https://gitcode.com/openharmony-tpc/rebound/issues) 给组件，当然，也非常欢迎给组件发[PR](https://gitcode.com/openharmony-tpc/rebound/pulls)共建。

## 开源协议

本项目基于 [BSD License](https://gitcode.com/openharmony-tpc/rebound/blob/master/LICENSE)，请自由地享受和参与开源。

