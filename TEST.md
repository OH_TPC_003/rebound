# text-encoding单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/facebookarchive/rebound-js/examples)
进行单元测试

单元测试用例覆盖情况

|                           接口名                            |是否通过	|备注|
|:--------------------------------------------------------:|:---:|:---:|
|             frictionFromOrigamiValue(oValue)             |pass   |        |
|             origamiValueFromTension(tension)             |pass   |        |
|             tensionFromOrigamiValue(oValue)              |pass   |        |
|                  radiansToDegrees(rad)                   |pass   |        |
|                  degreesToRadians(deg)                   |pass   |        |
| mapValueInRange(value, fromLow, fromHigh, toLow, toHigh) |pass  |     |
|                   bind(func, context)                    |   pass  |          |
|                  extend(target, source)                  | pass |  |
|                      onFrame(func)                       | pass  |       |
|                 removeFirst(array, item)                 |  pass |          |
|                    getSpringSystem()                     |  pass |          |
|                          run()                           | pass  |          |
|                      step(timestep)                      | pass  |          |
|          normalize(value, startValue, endValue)          |  pass |          |
|               projectNormal(n, start, end)               |  pass |          |
|            linearInterpolation(t, start, end)            | pass  |          |
|         quadraticOutInterpolation(t, start, end)         | pass  |          |
|                      b3Friction1(x)                      | pass  |          |
|                      b3Friction2(x)                      |  pass |       |
|                      b3Friction3(x)                      | pass  |       |
|                   b3Nobounce(tension)                    | pass  |          |
|     fromOrigamiTensionAndFriction(tension, friction)     | pass  |         |
|        fromBouncinessAndSpeed(bounciness, speed)         | pass  |       |
|       coastingConfigWithOrigamiFriction(friction)        | pass  |       |
|             createSpring(tension, friction)              |  pass |       |
|   createSpringWithBouncinessAndSpeed(bounciness, peed)   |  pass |       |
|           createSpringWithConfig(springConfig)           | pass  |          |
|              setSpringConfig(springConfig)               |  pass |       |
|                    getSpringConfig()                     | pass  |          |
|                     isOvershooting()                     | pass |       |
|               advance(time, realDeltaTime)               |  pass |       |
|   notifyPositionUpdated(notifyActivate, notifyAtRest)    | pass  |          |
|                  systemShouldAdvance()                   |pass   |          |
|                        isAtRest()                        | pass  |      |
|                       setAtRest()                        |  pass |       |
|                      getListeners()                      |  pass |       |
|                 addListener(newListener)                 |  pass |       |
|             removeListener(listenerToRemove)             |  pass |       |
|                   removeAllListeners()                   |  pass |       |
|            currentValueIsApproximately(value)            |  pass |       |
|                    setLooper(looper)                     |  pass |       |
|             createSpring(tension, friction)              |  pass |       |
|  createSpringWithBouncinessAndSpeed(bounciness, speed)   |  pass |       |
|           createSpringWithConfig(springConfig)           |  pass |       |
|                       getIsIdle()                        |  pass |       |
|                    getSpringById(id)                     |  pass |       |
|                     getAllSprings()                      |  pass |       |
|                  registerSpring(spring)                  |  pass |       |
|                 deregisterSpring(spring)                 |  pass |       |
|                 loop(currentTimeMillis)                  |  pass |       |
|                 activateSpring(springId)                 |  pass |       |
|                  addListener(listener)                   |  pass |       |
|                 removeListener(listener)                 |  pass |       |
|                   removeAllListeners()                   |  pass |       |

