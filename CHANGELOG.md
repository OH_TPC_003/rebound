## 2.0.1-rc.0

- Add interface declaration file index.d.ts

## 2.0.0

- DevEco Studio version: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API11 (4.1.0.36)
- ArkTs syntax adaptation

## 1.0.5

- Configured with new Hvigor build system for api9 stageMode.

## 1.0.4

- Configured with new Hvigor build system.

- Fixed issue, where SpringConfig.fromOrigamiTensionAndFriction api behaviour not proper, 0 value conversion were are not handled, now handled properly.

- getIdle api value is not returning correct value, status update was missing, status update added.



## 1.0.0

- Rebound's original code from Facebook is ported to OpenHarmony.

- Rebound library supports Spring dynamics Animation for OpenHarmony.
